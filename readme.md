# Aufgabe A - Jobs verteilt ausführen

- Anleitung von Kubernetes.io befolgen: https://kubernetes.io/docs/tasks/job/coarse-parallel-processing-work-queue/

  Dies beinhaltet die Installation und Erstellung einer Job-Queue mit dem Namen "job1". 
  Diese Job-Queue läuft auf einem separaten Container und ist jederzeit über den Abruf "amqp://guest:guest@rabbitmq-service:5672" verfügbar. Die Queue wird danach aus einem weiteren temporären Container mit 8 Texten/Wörtern befüllt, sodass 8 Inputs in der Queue zur Verarbeitung bereitliegen.

- Dieses Beispiel verwendet Python und ein Python-Script, welches beim Ausführen des Jobs bzw. der Pods auf allen Pods ausgeführt wird.

- Python-Script "skript.py":

```
#!/usr/bin/env python

import subprocess

bashCommand = "wget shorturl.at/bE156"
process = subprocess.Popen(bashCommand.split(), stdout=subprocess.PIPE)
output, error = process.communicate()
```

  Dieses Skript ruft über wget die folgende URL auf: shorturl.at/bE156
  Dabei wird auf dieser URL einen Counter um 1 erhöht, sodass überprüft werden kann, ob die URL auch wirklich 8 mal von den verteilt ausgeführten Jobs aufgerufen wurde.

- Die Container werden mit dem Dockerfile mit python und wget ausgestattet und dabei wird angegeben, dass die Python Befehle im skript.py ausgeführt werden sollen.

- dockerfile:

```
# Specify BROKER_URL and QUEUE when running
FROM ubuntu:18.04

RUN apt-get update && \
    apt-get install -y curl ca-certificates amqp-tools python wget \
       --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*
COPY ./skript.py /skript.py

CMD  /usr/bin/amqp-consume --url=$BROKER_URL -q $QUEUE -c 1 /skript.py
```

- Die Container Repo wird erstellt ("docker build" -Befehl) und auf gitlab.com gepusht mit folgenden Befehlen:

  **sudo docker build -t registry.gitlab.com/cntmodul/job-gruppe1-1** .
  
  **sudo docker push registry.gitlab.com/cntmodul/job-gruppe1-1**


- Die definition des Jobs mit namen "**job-gruppe1-1**", welcher die Pods - maximal 8 davon, 2 aufs Mal - anhand der Job-Queue erstellt, wird mittels yaml Datei gemacht. Die Container für die Pods werden direkt aus dem gitlab.com Repo geladen.

- job-gruppe1.yaml:

```
apiVersion: batch/v1
kind: Job
metadata:
  name: job-gruppe1-1
spec:
  completions: 8
  parallelism: 2
  template:
    metadata:
      name: job-gruppe1-1
    spec:
      containers:
      - name: c
        image: registry.gitlab.com/cntmodul/job-gruppe1-1
        env:
        - name: BROKER_URL
          value: amqp://guest:guest@rabbitmq-service:5672
        - name: QUEUE
          value: job1
      restartPolicy: OnFailure
```

- Danach wird der Job mit folgendem Befehl gestartet:

  **kubectl apply -f ./job-gruppe1.yaml**

- Die Ausführung kann übers Kobernetes Dashboard oder auch mit folgendem Befehl kontrolliert werden:

   **kubectl describe jobs/job-gruppe1-1**

- Nach erfolgreicher Ausführung ist der Counter auf der URL shorturl.at/bE156 um 8 gestiegen.



# Aufgabe B Health Probe Pattern


## Livenessprobe

Hier wird eine einfache Livenessprobe beschrieben.

In dem Beispiel wird ein Deployment erstellt, welches das Image busybox verwendet. 
Innerhalb der busybox wird dann die Shell geöffnet und und eine Datei namens "**Hello**" erstellt. 
Diese Datei wird nach 20 Sekunden gelöscht.

Die Livenessprobe wird mit dem Exec Command durchgeführt. Das Command ist cat Hello. Wenn der Exic Code dieses Kommandos 0 ist, wird die Livenessprobe als bestanden angesehen.

- livenessprobe.yaml:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: livenessprobe
  name: livenessprobe
spec:
  replicas: 1
  selector:
    matchLabels:
      app: livenessprobe
  template:
    metadata:
      labels:
        app: livenessprobe
    spec:
      containers:
      - image: busybox
        name: busybox
        args:
          - /bin/sh
          - -c
          - touch hello; sleep 20; rm -f hello; sleep 600
        livenessProbe:
          exec:
            command:
              - cat 
              - hello 
          initialDelaySeconds: 5
          periodSeconds: 3
```

Wir definieren am Schluss noch zwei Optionen:
- "**initialDelaySeconds**" welche auf 5 Sekunden sind. Dies bedeutet, dass wenn der Container startet, wird der erste Livenesscheck erst nach 5 Sekunde starten, damit noch keine Fehlermeldung entsteht, da der Container noch nicht oben ist.
- "**periodSeconds**" bedeutet, dass der Livenesscheck alle 3 Sekunden durchgeführt wird.

Der Ablauf ist nun wiefolgt: In den ersten 20 Sekunden nach erfolgreichem Start wird die Livenessprobe als erfolgreich angesehen und alles läuft einwandfrei.
Nach den 20 Sekunden wird die Livenessprobe fehlschlagen und der Kubernetescluster wird den Pod neu starten.

Wir starten das Deployment mit folgendem befehl:
- **kubectl create -f livenessprobe.yaml**

Anschliessend können wir prüfen, ob das Deployment gestartet ist:
- **kubectl get deploy**

Und in den Events überprüfen, was abgeht:
- **kubectl get Events**



## Readinessprobe

Mit der Readinessprobe verfolgen wir fast den gleichen Ansatz wie oben bei der Livenessprobe.

Im Unterschied zu Livenessprobe warten wir hier 20 Sekunden mit der Erstellung der "Hello" Datei. Wenn das Deployment startet wird somit die Readiness noch fehlschlagen. Erst nach diesen 20 Sekunden wird die "Hello" Datei erstellt und somit wird die Readiness auf Ready gestellt.

- readinessprobe.yaml:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    app: readinessprobe
  name: readinessprobe
spec:
  replicas: 1
  selector:
    matchLabels:
      app: readinessprobe
  template:
    metadata:
      labels:
        app: readinessprobe
    spec:
      containers:
      - image: busybox
        name: busybox
        args:
          - /bin/sh
          - -c
          - sleep 20; touch hello; sleep 600
        readinessProbe:
          exec:
            command:
              - cat 
              - hello 
          initialDelaySeconds: 15
          periodSeconds: 3
```

Wir starten das Deployment mit folgendem befehl:
- kubectl create -f readinessprobe.yaml

Wir prüfen, ob der Pod Ready ist:
kubectl get all

Anschliessend warten wir für ein paar Sekunden. Nach ca 20 Sekunden sollte der Pod Ready sein:
watch kubectl get all

# Aufgabe C - init Container

In diesem Beispiel gehen wir darauf ein, wie ein init Container verwendet werden kann. Die Idee in diesem Beispiel besteht darin, wass wir im init Container ein Shared Volume erstellen, in welchem eine HTML Datei generiert wird. Sobald dieses erstsellt wird, startet der eigentliche Webserver (Nginx), welches das vorbereitete Shared Volume im Stammverzeichnis von Nginx mounted:

- init_container_zahner.yaml:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  labels:
    run: nginx
  name: nginx-deploy
spec:
  replicas: 1
  selector:
    matchLabels:
      run: nginx
  template:
    metadata:
      labels:
        run: nginx

    spec:

      volumes:
      - name: shared-volume
        emptyDir: {}

      initContainers:
      - name: busybox
        image: busybox
        volumeMounts:
        - name: shared-volume
          mountPath: /nginx-data
        command: ["/bin/sh"]
        args: ["-c", "echo '<h1>Hello Kubernetes</h1>' > /nginx-data/index.html"]

      containers:
      - image: nginx
        name: nginx
        volumeMounts:
        - name: shared-volume
          mountPath: /usr/share/nginx/html
```

Wir starten das Deployment mit folgendem Befehl:
-  kubectl create -f init_container_zahner.yaml

Wir können den Status des Deployments mit folgendem Befehl überprüfen:
- kubectl get all

Sobald das Deployment namens "nginx-deploy" gestartet ist, können wir eine Portweiterleitung konfigurieren. Dies machen wir heir mit NodePort:
- kubectl expose deployment nginx-deploy --type NodePort --port 80

Wir können die Funktion überprüfen, indem wir nochmal folgenden Befehl eingeben:
- kubectl get service

Anschliessend können wir beim Service nginx-deploy den gemappten Port auslesen:
- nginx-deploy       NodePort       10.152.183.3     <none>        80:31883/TCP     35m

In unserem Fall wird der Port 80 auf Port 31883 gemappt. Nun müssen wir nur noch die WorkerNode IP Adresse in unserem Kluster nachschauen und den Port dahinter hängen. In unserem Fall können wir jetzt den Browser öffnen und folgende IP eingeben:
- http://10.1.39.5:31883/